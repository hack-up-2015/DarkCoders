<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Juan Kultura</title>
	
    <!-- css -->
    <link rel="stylesheet" href="<?php echo base_url()?>css/bootstrap.min.css">
    <link href="<?php echo base_url()?>css/nivo-lightbox.css" rel="stylesheet" />
	<link href="<?php echo base_url()?>css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo base_url()?>css/animations.css" rel="stylesheet" />
    <link href="<?php echo base_url()?>css/style.css" rel="stylesheet">
	<link href="<?php echo base_url()?>color/default.css" rel="stylesheet">

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	
	<section class="hero2" id="intro">
            <div class="container">
              <div class="row">
                <div class="col-md-12 text-right navicon">
                  <a id="nav-toggle" class="nav_slide_button" href="#"><span></span></a>
                </div>
              </div>
              <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center inner">
					<div class="animatedParent">
						<h1 class="animated fadeInDown">JUAN KULTURA RELIHIYON</h1>
						<p class="animated fadeInUp">Ang Relihiyon ni Juan noon at ngayon</p>
					</div>
			   </div>
              </div>
              <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center">
                  <a href="#about" class="learn-more-btn btn-scroll">Alamin natin ito!</a>
                </div>
              </div>
            </div>
    </section>
	
	
    <!-- Navigation -->
    <div id="navigation">
        <nav class="navbar navbar-custom" role="navigation">
                              <div class="container">
                                    <div class="row">
                                          <div class="col-md-2">
                                                   <div class="site-logo">
                                                            <a href="#" class="brand">Juan Kultura Relihiyon</a>
                                                    </div>
                                          </div>
                                          

                                          <div class="col-md-10">
                         
                                                      <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                                                <i class="fa fa-bars"></i>
                                                </button>
                                          </div>
                                                      <!-- Collect the nav links, forms, and other content for toggling -->
                                                      <div class="collapse navbar-collapse" id="menu">
                                                            <ul class="nav navbar-nav navbar-right">
                                                                  <li class="active"><a href="#intro">Panimula</a></li>
                                                                  <li><a href="#about">Layunin</a></li>
								<li><a href="#works">Mga Nagagawa</a></li>				                                                                  
                                                                  <li><a href="<?php echo base_url()?>">Bumalik</a></li>
                                                            </ul>
                                                      </div>
                                                      <!-- /.Navbar-collapse -->
                             
                                          </div>
                                    </div>
                              </div>
                              <!-- /.container -->
                        </nav>
    </div> 
    <!-- /Navigation -->  

	<!-- Section: about -->
    <section id="about" class="home-section color-dark bg-white">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="animatedParent">
					<div class="section-heading text-center animated bounceInDown">
					<h2 class="h-bold">Relihiyon</h2>
					<div class="divider-header"></div>
					</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">

		
        <div class="row">
		
		
            <div class="col-lg-8 col-lg-offset-2 animatedParent">		
				<div class="text-center">
					<p>
					Sabi nga naman nila, malikhain ang bawat Pilipino. At totoo ito dahil sa mga iba't-ibang kaugalian ng mga Pinoy pagdating sa pagsisimba.
					</p>
					<p>
					Sapagkat bawat Pilipino ay maraming resposibilidad sa buhay, hinding-hindi mawawala ang kanilang pananampalataya sa Diyos.
					</p>
					<a href="#works" class="btn btn-skin btn-scroll">Nais pang magsaliksik</a>
				</div>
            </div>
		

        </div>		
		</div>

	</section>
	<!-- /Section: about -->
	
	
	<!-- Section: services -->
    
	<!-- /Section: services -->
	

	<!-- Section: works -->
    <section id="works" class="home-section color-dark text-center bg-white">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div>
					<div class="animatedParent">
					<div class="section-heading text-center">
					<h2 class="h-bold animated bounceInDown">Mga larawan ng iba't-ibang relihiyon sa Pilipinas</h2>
					<div class="divider-header"></div>
					</div>
					</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">

            <div class="row animatedParent">
                <div class="col-sm-12 col-md-12 col-lg-12" >

                    <div class="row gallery-item">
                        <div class="col-md-3 animated fadeInUp">
							<a href="<?php echo base_url()?>img/works/r1.png" title="Roman Catholic" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="<?php echo base_url()?>img/works/r1.png" class="img-responsive" alt="img">
							</a>
						</div>
						<div class="col-md-3 animated fadeInUp slow">
                                                    <a href="<?php echo base_url()?>img/works/r4.jpg" title="Church of God" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="<?php echo base_url()?>img/works/r4.jpg" class="img-responsive" alt="img">
							</a>
						</div>
						<div class="col-md-3 animated fadeInUp slower">
							<a href="<?php echo base_url()?>img/works/r5.jpg" title="Church of God" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="<?php echo base_url()?>img/works/r5.jpg" class="img-responsive" alt="img">
							</a>
						</div>
						<div class="col-md-3 animated fadeInUp">
							<a href="<?php echo base_url()?>img/works/r6.jpg" title="Iglesia Ni Kristo" data-lightbox-gallery="gallery1" data-lightbox-hidpi="img/works/1@2x.jpg">
								<img src="<?php echo base_url()?>img/works/r6.jpg" class="img-responsive" alt="img">
							</a>
						</div>
					</div>
	
                </div>
            </div>          
		</div>

	</section>
	<!-- /Section: works -->

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="footer-menu">
						<li><a href="#">Home</a></li>
						<li><a href="#">Press release</a></li>
					</ul>
				</div>
				<div class="col-md-6 text-right">
					<p>&copy;Copyright 2015 Dark Coders HackUP</p>
                    <!-- 
                        All links in the footer should remain intact. 
                        Licenseing information is available at: http://bootstraptaste.com/license/
                        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Bocor
                    -->
				</div>
			</div>	
		</div>
	</footer>

    <!-- Core JavaScript Files -->
    <script src="<?php echo base_url()?>js/jquery.min.js"></script>	 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url()?>js/jquery.sticky.js"></script>
    <script src="<?php echo base_url()?>js/jquery.easing.min.js"></script>	
	<script src="<?php echo base_url()?>js/jquery.scrollTo.js"></script>
	<script src="<?php echo base_url()?>js/jquery.appear.js"></script>
	<script src="<?php echo base_url()?>js/stellar.js"></script>
	<script src="<?php echo base_url()?>js/nivo-lightbox.min.js"></script>
	
    <script src="<?php echo base_url()?>js/custom.js"></script>
	<script src="<?php echo base_url()?>js/css3-animate-it.js"></script>

</body>

</html>
